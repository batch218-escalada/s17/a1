/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

    let givePersonalInfo = function printPersonalInfo(){
        let fullName = prompt('Enter your full name:');
        let age = prompt('Enter your age:');
        let location = prompt('Enter your location:');

        console.log('Hello, ' + fullName);
        console.log('You are ' + age + ' years old.');
        console.log('You live in ' + location);
    };

    givePersonalInfo();

/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

    let listFavoriteBand = function listBands(){
        let band1 = 'Linkin Park';
        let band2 = 'RADWIMPS';
        let band3 = 'We The Kings';
        let band4 = 'Parokya Ni Edgar';
        let band5 = 'Brownman Revival';

        console.log('1. ' + band1);
        console.log('2. ' + band2);
        console.log('3. ' + band3);
        console.log('4. ' + band4);
        console.log('5. ' + band5);
    };

    listFavoriteBand();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    let listFavoriteMovies = function listMovies(){
        let movie1 = 'Avatar (2009)';
        let movie2 = 'The Lord of the Rings: The Return of the King (2003)';
        let movie3 = 'The Lord of the Rings: The Fellowshif of the Ring (2001)';
        let movie4 = 'The Lord of the Rings: The Two Towers (2002)';
        let movie5 = 'Avengers: Endgame (2019)';
    
        console.log('1. ' + movie1);
        console.log('Rotten Tomatoes Rating: 82%')
        console.log('2. ' + movie2);
        console.log('Rotten Tomatoes Rating: 93%')
        console.log('3. ' + movie3);
        console.log('Rotten Tomatoes Rating: 91%')
        console.log('4. ' + movie4);
        console.log('Rotten Tomatoes Rating: 95%')
        console.log('5. ' + movie5);
        console.log('Rotten Tomatoes Rating: 94%')
    };

    listFavoriteMovies();

/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

    let printFriends = function printUsers(){
        alert("Hi! Please add the names of your friends.");
        let friend1 = prompt("Enter your first friend's name:"); 
        let friend2 = prompt("Enter your second friend's name:"); 
        let friend3 = prompt("Enter your third friend's name:");

        console.log("You are friends with:")
        console.log(friend1); 
        console.log(friend2); 
        console.log(friend3); 
    };

    printFriends();

// console.log(friend1);
// console.log(friend2);
